import Card from './Card.js';
import Game from './Game.js';
import { setSpeedRate as setGameSpeedRate } from './SpeedRate.js';

// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}

// Отвечает является ли карта братком.
function isLad(card) {
    return card instanceof Lad;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isLad(card)) {
        return 'Чем их больше, тем они сильнее';
    }
    if (isDog(card)) {
        return 'Собака';
    }
    return 'Существо';
}

// Основа для существ.
class Creature extends Card {
    constructor(name, maxPower, image) {
        super(name, maxPower, image);
        this.maxPower = maxPower;
    }
    getDescriptions() {
        return [
            getCreatureDescription(this),
            super.getDescriptions()
        ];
    };
    get currentPower() {
        return this._currentPower;
    }
    set currentPower(value) {
        this._currentPower = value > this.maxPower ? this.maxPower : value; 
    } 
}

// Основа для утки.
class Duck extends Creature {
    constructor(name = 'Мирная утка', maxPower = 2, image = 'duck.png') {
        super(name, maxPower, image);
    }
    quacks() { 
        console.log('quack') 
    };

    swims() { 
        console.log('float: both;') 
    };
}

// Основа для собаки.
class Dog extends Creature {
    constructor(name = 'Пес-бандит', maxPower = 3, image = 'dog.png') {
        super(name, maxPower, image);
    }
    
    swims() { 
        console.log('float: none;') 
    };
}

// Основа для братков.
class Lad extends Dog {
    constructor(name = 'Браток', maxPower = 2, image = 'lad.jpg') {
        super(name, maxPower, image);
    }
    static getInGameCount() {
        return this.inGameCount || 0;
    }
    static setInGameCount(value) { 
        this.inGameCount = value; 
    }
    static getBonus() {
        return this.getInGameCount() * (this.getInGameCount() + 1) / 2;
    }
    modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
        continuation(value + Lad.getBonus());
    };
    modifyTakenDamage(value, fromCard, gameContext, continuation) {
        continuation(value - Lad.getBonus());
    };

    doAfterComingIntoPlay(gameContext, continuation) {
        Lad.setInGameCount(Lad.getInGameCount() + 1);
        continuation();
    };
    doBeforeRemoving(continuation) {
        Lad.setInGameCount(Lad.getInGameCount() - 1);
        continuation();
    };
}

// Основа для изгоя
class Rogue extends Creature {
    constructor(name = 'Изгой', maxPower = 2, image = 'rogue.png') {
        super(name, maxPower, image);
    }
    
    doBeforeAttack(gameContext, continuation) {
        const ability = ['modifyDealedDamageToCreature', 'modifyDealedDamageToPlayer', 'modifyTakenDamage'];
        const {oppositePlayer, position, updateView} = gameContext;
        const oppositeCardName = oppositePlayer.table[position].name;
        
        oppositePlayer.table.forEach(card => {
            if (oppositeCardName === card.name) {
                const cardPrototype = Object.getPrototypeOf(card);
                const prototypeKeys = Object.getOwnPropertyNames(cardPrototype);
                prototypeKeys.forEach(key => {
                    if (ability.includes(key)) {
                        this[key] = cardPrototype[key];
                        delete cardPrototype[key];
                    }
                })
            }
        })

        updateView();
        continuation();
    };
}

// Основа для пивовара.
class Brewer extends Duck {
    constructor(name = 'Пивовар', maxPower = 2, image = 'brewer.png') {
        super(name, maxPower, image);
    }

    doBeforeAttack(gameContext, continuation) {
        const {currentPlayer, oppositePlayer} = gameContext;
        const allCards = currentPlayer.table.concat(oppositePlayer.table)
        allCards.forEach(card => {
            if (isDuck(card)) {                
                card.maxPower++;
                card.currentPower += 2;
                
                card.view.signalHeal(continuation);
                card.updateView();
            }
        });
    };
}

// Основа для немо.
class Nemo extends Creature {
    constructor(name = 'Немо', maxPower = 4, image = 'nemo.jpg') {
        super(name, maxPower, image);
    }

    doBeforeAttack(gameContext, continuation) {
        const {oppositePlayer, position, updateView} = gameContext;
        const oppositeCard = oppositePlayer.table[position];
        const oppositeCardPrototype = Object.getPrototypeOf(oppositeCard);

        Object.setPrototypeOf(this, oppositeCardPrototype);
        super.doBeforeAttack(gameContext, continuation);
        
        const yourCardPrototype = Object.getPrototypeOf(this);
        delete yourCardPrototype.doBeforeAttack;
        delete yourCardPrototype.quacks;
        delete yourCardPrototype.swim;

        updateView();
    }
}

// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [new Nemo(), new Duck()];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [new Brewer(), new Lad()];

// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
setGameSpeedRate(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
